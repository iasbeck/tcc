package Aquisition;

import Control.PID;
import Motors.NewMotor;
import Sensors.Compass;
import USB.USBComm;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;
import lejos.util.Delay;

/**
 * Esta classe tem como objetivo a implementa��o de uma aquisi��o em malha
 * fechada. O objetivo aqui � determinar a planta de um seguidor que utiliza
 * b�ssola, ou seja, a rela��o entre a entrada (pot�ncia no motor) e a sa�da
 * (valor da b�ssola) da planta. Anteriormente tentamos realizar a aquisi��o da
 * planta em malha aberta, injetando valores de pot�ncia de motor diversos no
 * seguidor e analisando a varia��o da b�ssola, por�m, a planta que obtemos s�
 * convergia com um controlador com par�metros negativos, enquanto que o
 * seguidor facilmente se estabilizava com um simples kp de 0,8. Chegamos �
 * conclus�o que nosso modelo em malha aberta n�o estava representando bem a
 * planta e decidimos realizar um modelo em malha fechada.
 * 
 * A ideia � utilizar um controlador com kp = 0,8 e ir mudando a refer�ncia ao
 * longo do tempo para poder medir diversas uma s�rie de entradas diferentes no
 * meu sistema e poder obter uma modelagem mais condizente com a realidade.
 * 
 * @author Arthur Iasbeck
 *
 */

public class AquisitionCloseLoop {

	public static final boolean USB_COMM = true; // Set para false para desativar a comunica��o USB
	public static final boolean MOTORS = true; // Set para false para desativar os motores
	public static final long SIM_TIME = 10; // Tempo da simula��o em ms
	public static final long REFERENCE_TIME = 2000; // Tempo que leva para que a refer�ncia seja alterada
	public static final double KP = 0.8;
	public static final double KI = 0;
	public static final double KD = 0;
	
	public static final int MIN_POWER = 40;
	public static final String TIME_MSG = "A";
	public static final String COMPASS_MSG = "B";
	public static final String DELTA_COMPASS_MSG = "C";
	

	public static void main(String args[]) {
		USBComm usb = new USBComm(); // Objeto para comunica��o USB
		Compass compass = new Compass(SensorPort.S1); // Objeto para o sensor RGB
		NewMotor motorA = new NewMotor(MotorPort.A, true); // Representa o motor A
		NewMotor motorB = new NewMotor(MotorPort.B, true); // Representa o motor B
		PID pid;
		long time = 0; // Tempo de in�cio da simula��o
		long lastInputChangeTime = 0; // Tempo em que a entrada foi variada pela �ltima vez
		int controlPowerA = 0; // A��o de controle sobre o motor A
		int controlPowerB = 0; // A��o de controle sobre o motor B
		double compassValue = 0; // Valor da b�ssola a cada itera��o
		double lastCompassValue = 0; // �ltimo valor lido pela b�ssola
		double deltaCompass; // Varia��o dos valores de �ngulo da b�ssola
		double input = 0; // Entrada da planta
		String usbMsg = ""; // Mensagem enviada ao PC atrav�s do USB
		boolean firstLoop = true; // Informa se estamos no primeiro loop
		long pastTime = 0; // Tempo total de simula��o
		double ref; // Refer�ncia para o controlador (seguidor)
		double[] refArray = new double[4];
		int refIndex = 0;

		/*
		 * Esta fun��o � iniciada e aguarda at� que o computador estabele�a com o CLP a
		 * comunica��o USB.
		 */
		if (USB_COMM) {
			usb.connect();
		}

		/*
		 * Aqui aguardamos at� que algum bot�o seja precionado para iniciar a simula��o.
		 */
		LCD.clear();
		LCD.drawString("Pressione para \niniciar a \nsimulacao...", 0, 0);
		Button.waitForAnyPress();
		while (Button.readButtons() != 0);

		LCD.clear();
		LCD.drawString("Simulacao \niniciada !", 0, 0);

		refArray[0] = compass.getSensor();
		refArray[1] = refArray[0] + 90;
		if(refArray[1] >= 360) refArray[1] -= 360;
		refArray[2] = refArray[0] + 90;
		if(refArray[2] >= 360) refArray[2] -= 360;
		refArray[3] = refArray[0] + 90;
		if(refArray[3] >= 360) refArray[3] -= 360;
		
		LCD.drawString("" + refArray[0], 0, 0);
		LCD.drawString("" + refArray[1], 0, 1);
		LCD.drawString("" + refArray[2], 0, 2);
		LCD.drawString("" + refArray[3], 0, 3);
		
		ref = refArray[0];
		pid = new PID(ref, KP, KI, KD);
		lastInputChangeTime = System.currentTimeMillis();
		/*
		 * Aqui se inicia a simula��o.
		 */
		while (!Button.ENTER.isDown()) {
			/*
			 * Controle do seguidor.
			 */
			if (System.currentTimeMillis() - lastInputChangeTime > REFERENCE_TIME) {
				/*
				 * Este if � respons�vel por alterar a refer�ncia a cada REFERENCE_TIME ms.
				 */
				lastInputChangeTime = System.currentTimeMillis();
				refIndex++;
				if(refIndex > 3) refIndex = 0;
				ref = refArray[refIndex];
			}
			compassValue = compass.getSensor();
			deltaCompass = compassValue - lastCompassValue;
			input = pid.compute(compassValue);
			controlPowerA = MIN_POWER - (int) Math.round(input);
			controlPowerB = MIN_POWER + (int) Math.round(input);
			if (controlPowerA > 100) {
				controlPowerA = 100;
			}
			if (controlPowerA < -100) {
				controlPowerA = -100;
			}
			if (!MOTORS) {
				controlPowerA = 0;
				controlPowerB = 0;
			}
			motorA.setPower(controlPowerA);
			motorA.forward();
			motorB.setPower(controlPowerB);
			motorB.forward();

			/*
			 * Envio da mensagem.
			 */
			if (USB_COMM) {
				usbMsg = input + COMPASS_MSG + compassValue + DELTA_COMPASS_MSG + deltaCompass + TIME_MSG;
			}
			if (firstLoop) {
				time = System.currentTimeMillis();
				pastTime = System.currentTimeMillis() - time;
				usbMsg += pastTime;
				firstLoop = false;
			} else {
				while ((System.currentTimeMillis() - time) < (pastTime + 50));
				pastTime = System.currentTimeMillis() - time;
				usbMsg += pastTime;
			}
			if (USB_COMM) {
				usb.send(usbMsg);
			}
			LCD.drawString("comp=" + compassValue, 0, 0);
			LCD.drawString("delta=" + deltaCompass, 0, 1);
			LCD.drawString("time=" + time, 0, 2);
			LCD.drawString("pastT=" + pastTime, 0, 3);
			if (pastTime >= SIM_TIME * 1000) {
				break;
			}
			lastCompassValue = compassValue;
		}
		
		/*
		 * Finalizando a execu��o.
		 */
		while (Button.readButtons() != 0);
		motorA.stop();
		motorB.stop();
		if (USB_COMM) {
			usbMsg = "*";
			usb.send(usbMsg);
		}
		Delay.msDelay(50);
		LCD.clear();
		LCD.drawString("Fim da simulacao \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}
}

