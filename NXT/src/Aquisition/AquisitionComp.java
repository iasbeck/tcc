package Aquisition;

import Motors.NewMotor;
import Sensors.Compass;
import USB.USBComm;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;
import lejos.util.Delay;

public class AquisitionComp {

	public static final boolean USB_COMM = true; // Set para false para desativar a comunica��o USB
	public static final boolean MOTORS = true; // Set para false para desativar os motores
	public static final long SIM_TIME = 10; // Tempo da simula��o em ms
	public static final long INPUT_TIME = 1000; // Tempo que leva para que a entrada seja alterada
	public static final int MAX_COMPASS = 180; // Valor m�ximo que a b�ssola pode assumir
	public static final int MIN_COMPASS = 40; // Valor m�nimo que a b�ssola pode assumir

	public static final int MIN_POWER = 40;

	public static final String TIME_MSG = "A";
	public static final String COMPASS_MSG = "B";
	public static final String DELTA_COMPASS_MSG = "C";

	public static void main(String args[]) {
		USBComm usb = new USBComm(); // Objeto para comunica��o USB
		Compass compass = new Compass(SensorPort.S1); // Objeto para o sensor RGB
		NewMotor motorA = new NewMotor(MotorPort.A, true); // Representa o motor A
		NewMotor motorB = new NewMotor(MotorPort.B, true); // Representa o motor B
		long time = 0; // Tempo de in�cio da simula��o
		long stopTime = 0; // Tempo que o seguidor fica parado
		long lastInputChangeTime = 0; // Tempo em que a entrada foi variada pela �ltima vez
		int controlPowerA = 0; // A��o de controle sobre o motor A
		int controlPowerB = 0; // A��o de controle sobre o motor B
		double compassValue = 0; // Valor da b�ssola a cada itera��o
		double lastCompassValue = 0; // �ltimo valor lido pela b�ssola
		double input = 0; // Entrada da planta
		String usbMsg = ""; // Mensagem enviada ao PC atrav�s do USB
		boolean firstLoop = true; // Informa se estamos no primeiro loop
		long pastTime = 0; // Tempo total de simula��o
		int inputSignal = 1; // Vari�vel que determina se a entrada ser� positiva ou negativa
		int inputMultiplier = 0;

		/*
		 * Esta fun��o � iniciada e aguarda at� que o computador estabele�a com o CLP a
		 * comunica��o USB.
		 */
		if (USB_COMM) {
			usb.connect();
		}

		/*
		 * Aqui aguardamos at� que algum bot�o seja precionado para iniciar a simula��o.
		 */
		LCD.clear();
		LCD.drawString("Pressione para \niniciar a \nsimulacao...", 0, 0);
		Button.waitForAnyPress();
		while (Button.readButtons() != 0);

		LCD.clear();
		LCD.drawString("Simulacao \niniciada !", 0, 0);

		lastInputChangeTime = System.currentTimeMillis();
		/*
		 * Aqui se inicia a simula��o.
		 */
		while (!Button.ESCAPE.isDown()) {
			/*
			 * Caso o bot�o ENTER seja pressionado, a contagem de tempo pausa e os motores
			 * s�o desligados.
			 */
			if (Button.ENTER.isDown()) {
				/*
				 * Para que o tempo em que estive pausado n�o seja refletido no tempo total de
				 * execu��o do programa, eu conto quanto tempo fiquei pausado e em seguida
				 * adiciono este tempo � vari�vel time, respons�vel por contar o tempo, para que
				 * a subtra��o (System.currentTimeMillis() - time) continue exatamente como
				 * estava antes.
				 */
				stopTime = System.currentTimeMillis();
				while (Button.ENTER.isDown());
				/*
				 * Quando estou pausado preciso parar os motores
				 */
				motorA.stop();
				motorB.stop();
				LCD.clear();
				LCD.drawString("Pressione para \ncontinuar...", 0, 0);
				while (!Button.ENTER.isDown() && !Button.ESCAPE.isDown());
				if (Button.ESCAPE.isDown()) {
					while (Button.ESCAPE.isDown());
					break;
				} else if (Button.ENTER.isDown()) {
					while (Button.ENTER.isDown());
					time += System.currentTimeMillis() - stopTime;
				}
				LCD.clear();
			}

			/*
			 * Controle do seguidor
			 */
			/*
			 * O input gerado possui 6 valores poss�vel (10,20,30,-10,-20,-30). A cada vez
			 * que o input � alterado, um n�mero aleat�rio entre 1 e 3 � gerado. O mesmo �
			 * ent�o multiplicado por 10 e pelo valor inputSignal. A cada vez que a entrada
			 * � alterado, o sinal de inputSignal � invertido. Inicialmente a entrada assume
			 * um valor dentre (10,20,30), em seguida um valor dentre (-10,-20,-30) e assim
			 * sucessivamente. A entrada � alterada a cada INPUT_TIME milissegundos ou caso
			 * o bot�o direito seja pressionado.
			 */
			compassValue = compass.getSensor();
			if (System.currentTimeMillis() - lastInputChangeTime > INPUT_TIME || Button.RIGHT.isDown()) {
				while(Button.RIGHT.isDown());
				inputMultiplier = (int) Math.round(Math.random() * 2.0 + 1.0);
				input = (int) Math.round(10 * inputSignal);
				if (inputSignal == 1) inputSignal = -1;
				else if (inputSignal == -1) inputSignal = 1;
				lastInputChangeTime = System.currentTimeMillis();
			}
			controlPowerA = MIN_POWER + (int) Math.round(input);
			controlPowerB = MIN_POWER - (int) Math.round(input);
			if (controlPowerA > 100) {
				controlPowerA = 100;
			}
			if (controlPowerA < -100) {
				controlPowerA = -100;
			}
			if (!MOTORS) {
				controlPowerA = 0;
				controlPowerB = 0;
			}
			motorA.setPower(controlPowerA);
			motorA.forward();
			motorB.setPower(controlPowerB);
			motorB.forward();

			/*
			 * Envio da mensagem
			 */
			if (USB_COMM) {
				usbMsg = input + COMPASS_MSG + compassValue + TIME_MSG;
			}
			if (firstLoop) {
				time = System.currentTimeMillis();
				pastTime = System.currentTimeMillis() - time;
				usbMsg += pastTime;
				firstLoop = false;
			} else {
				while ((System.currentTimeMillis() - time) < (pastTime + 50));
				pastTime = System.currentTimeMillis() - time;
				usbMsg += pastTime;
			}
			if (USB_COMM) {
				usb.send(usbMsg);
			}
			LCD.drawString("comp=" + compassValue, 0, 0);
			LCD.drawString("time=" + time, 0, 1);
			LCD.drawString("pastT=" + pastTime, 0, 2);
			if (pastTime >= SIM_TIME * 1000) {
				break;
			}
			lastCompassValue = compassValue;
		}
		while (Button.readButtons() != 0);
		motorA.stop();
		motorB.stop();
		if (USB_COMM) {
			usbMsg = "*";
			usb.send(usbMsg);
		}
		Delay.msDelay(50);
		LCD.clear();
		LCD.drawString("Fim da simulacao \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}
}
